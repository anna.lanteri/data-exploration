# Data Exploration

This project contains single jupyter notebooks with utils and examples for data exploration of CMIP6 data sets.

### Data
The data is stored in [netSCDF][1] files. The data accessed in these scripts is stored on Levante. The convention for the titles of the data files is the following:

>     /pool/data/MESSY/DATA/MESSy2/raw/offemis/CMIP6/CMIP6v1.1_DLR1.0_<ssp scenario>_road_MISC_<yyyymm start date>-<yyyymm end date>.nc
 
    
so for example the file containing CMIP6 road emissions for ssp370 between January 2016 and December 2015 would be called: 
    
>    CMIP6v1.1_DLR1.0_ssp370_road_MISC_201501-201512.nc    
    
The time ranges available are the following:     

The singular years from 2015 to 2020:
> CMIP6v1.1_DLR1.0_ssp370_road_MISC_201501-201512.nc 
    
    ...
    
> CMIP6v1.1_DLR1.0_ssp370_road_MISC_202001-202012.nc
    
The years from 2015 to 2100:

> CMIP6v1.1_DLR1.0_ssp370_road_MISC_201501-210012.nc
    
The decades from the 2000 twnties to the nineties:

> CMIP6v1.1_DLR1.0_ssp370_road_MISC_202101-203012.nc

    ...
    
> CMIP6v1.1_DLR1.0_ssp370_road_MISC_209101-210012.nc
    
    
### Scripts
    
* **data_explorations.ipynb**: How to open and read an example .nc file, and get some basic information from the dataset.
* **visualization.ipynb**: How to plot the data, some colormaps examples
* **cartopy_basemap.ipynb**: How to plot data using cartopy, adding coastlines, using projecttions
* **zonal_average.ipynb**: how to calculate the yearly zonal average total emission of each chemical specie and why it matters
* **inventories_comparison.ipnyb**: Some different ways to plot comparing different values across inventories. Values taken from [ECCAD][2]
* **scenarios_comparison.ipnyb**: Comparing values in the available data across [SSP scenarios][3] and across the years
    

    
    
    
    
    
    
    
[1]: <https://www.unidata.ucar.edu/software/netcdf/>
[2]: <https://eccad.sedoo.fr/>
[3]: <https://www.dkrz.de/en/communication/climate-simulations/cmip6-en/the-ssp-scenarios>