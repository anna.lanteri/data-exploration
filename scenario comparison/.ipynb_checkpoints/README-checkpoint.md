# Data Exploration

This project contains single jupyter notebooks with utils and examples for data exploration of CMIP6 data sets.

## Data
The data accessed in these scripts is stored on Levante. The convention for the titles of the data files is the following:

 '/pool/data/MESSY/DATA/MESSy2/raw/offemis/CMIP6/CMIP6v1.1_DLR1.0_ssp370_road_MISC_201501-201512.nc'